# The Wheelchair project - The new body of the disabled
#### Korean Minjok Leadership Academy 2022-1 Integrated project member,<br><br> Jinho Kim, Seunghyun Park, Hyunmin Lee, Donggeon Kim <br>
Since Oct. 2021

## What We Do
We make wheelchairs for disabled people.<br>
#### The Features are: <br>
+ Moving wheelchair via simple handsigns
+ Making GUI program that can be operated with eye-trackers
+ ###### The features of the GUI programs are:
  + Speaking via text-to-speech technology
  + Sending SMS
  + Map

## What we use

#### Programming Languages We Use
+ Python
+ C++ (Arduino)
+ Typescript

#### Technologies We Use
+ Arduino Circuits
  + 500W/36V DC Motor
  + Motordrivers
+ **Naver** Cloud Service APIs:
  + SMS API
  + TTS API
+ **Tkinter** Python Library
+ **Mediapipe** Python Library
+ Tobii Eye Tracker
___

#### To help the disabled with our best, This repository is fully opened by open source.
#### This repository and project is published on KSEF (Korea Science Engineering Fair) 2021.
#### Once this repository is released via opensource, all of the codes and schematics, etc., will be in reach of BSD 2-Clause "Simplified" License.

### How To Build
